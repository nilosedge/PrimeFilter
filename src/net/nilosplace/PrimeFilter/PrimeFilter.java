package net.nilosplace.PrimeFilter;

import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.math.BigDecimal;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PrimeFilter extends JFrame {
	
	public PrimeFilter() {
		super("Prime Filter");
		String number = "1763998488000299";
		int primesize = (int)(number.length() / 2);
		
		int number1ar[] = new int[primesize];
		int number2ar[] = new int[primesize];
		int number1 = 0;
		int number2 = 0;
		JPanel contentPane = new JPanel();
		contentPane.setLayout();
		
		JTextField[] jt1 = new JTextField[primesize];
		JTextField[] jt2 = new JTextField[primesize];
		JLabel[] jl1 = new JLabel[primesize];
		
		
		for(int i = 0; i < primesize; i++) {
			jt1[i] = new JTextField(1);
			jl1[i] = new JLabel(String.valueOf(number1));
			jt1[i].setText(String.valueOf(number1ar[i]));
			jt1[i].addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						System.out.println("Action Happened Text Box1");
					}
				}
			);

			jt2[i] = new JTextField(1);
			jt2[i].setText(String.valueOf(number1ar[i]));
			jt2[i].addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						System.out.println("Action Happened Text Box2");
					}
				}
			);
			
			contentPane.add(jl1[i]);
			contentPane.add(jt1[i]);
			contentPane.add(jt2[i]);
		}
		
		//BigDecimal big = new BigDecimal("188198812920607963838697239461650439807163563379417382700763356422988859715234665485319060606504743045317388011303396716199692321205734031879550656996221305168759307650257059");
		BigDecimal big = new BigDecimal(number);
		
		System.out.println(big);
		
		setContentPane(contentPane);
		
        addWindowListener(new WindowAdapter() {
           public void windowClosing(WindowEvent ev) {
              System.exit(0);
           }
        });
        
        
        setSize(1100, 700);
		setVisible(true);
	}
	
    public static void main(String[] args) {
        PrimeFilter window = new PrimeFilter();
     }


}
